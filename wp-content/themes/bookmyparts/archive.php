<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package 7up-framework
 */

get_header(); ?>
<div id="main-content" class="main-wrapper archive-default page-default">
    <?php do_action('s7upf_before_main_content')?>
    <div class="container">
        <div class="row">
            <?php s7upf_output_sidebar('left')?>
            <div class="main-content <?php echo esc_attr(s7upf_get_main_class()); ?>">
                <div class="content-blog-page border radius">
                    <?php if(have_posts()):?>
                        <?php s7upf_paging_nav();?>
                        <div class="content-blog-large">
                            <?php the_archive_title('<h2 class="page-title">','</h2>'); ?>
                            <?php while (have_posts()) :the_post();?>

                                <?php get_template_part('s7upf_templates/blog-content/content');?>

                            <?php endwhile;?>

                        </div>
                        <?php s7upf_paging_nav('bottom');?>
                        <?php wp_reset_postdata();?>
                    <?php else : ?>
                        <?php get_template_part( 's7upf_templates/blog-content/content', 'none' ); ?>
                    <?php endif;?>
                    
                </div>
            </div>
            <?php s7upf_output_sidebar('right')?>
        </div>
    </div>
    <?php do_action('s7upf_after_main_content')?>
</div>
<?php get_footer(); ?>
