(function($,W,D)
{
    var JQUERY4U = {};

    JQUERY4U.UTIL =
    {
		
	
        setupFormValidation: function()
        {
			$.validator.addMethod("regex", function(value, element, regexpr) {          
				return regexpr.test(value);
		   	}, "Please enter a valid GST.");  
			
            $("#seller_registration").validate({
				rules: {
                billing_phone: { required: true,
                        minlength: 10
                    },
				},
				
				fname: { required: true },
				shopname: { required: true, minlength:10 },
				password: {	required: true },
				
				email: {
					required: true,
					email: true
				},
				
				gstno: { required: true },
   
                messages: {
                    billing_phone: "Please enter minimum 10 digits!",
					password: "Please enter account password.",
					fname: "Please enter name.",
					gstno: "Please enter GST number.",
					shopname: "Please enter Company name.",
                },
                submitHandler: function(form) {
                    form.submit();
                }
			
			
			
            });
			
			
        }
    }
    
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });



})(jQuery, window, document);