<?php
/*
// 
Template name: Registration Form 
//
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php get_header(); ?>

<div id="main-content" class="page-default">
       
        <div class="container">
           
            <div class="row">
               
                <div class="main-content <?php echo esc_attr(s7upf_get_main_class()); ?>">
                    <div class="content-single">
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    								<div class="content-detail-text clearfix">


<div class="woocommerce">


<?php
if(isset($_GET['err']) )
{
	
	$err = 	$_GET['err'];
	
	switch($err)
	{
			case 10:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong> Please enter Email address.</li><li><strong>Error:</strong> Please enter Password.</li><li><strong>Error:</strong> Please enter Phone number.</li></ul>');
			break;
			
			case 9:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong> Please enter Email address.</li><li><strong>Error:</strong> Please enter Phone number.</li></ul>');
			break;
			
			case 8:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong> Please enter Password.</li><li><strong>Error:</strong> Please enter Phone number.</li></ul>');
			break;
			
			case 7:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong> Please enter Email address.</li><li><strong>Error:</strong> Please enter Password.</li></ul>');
			break;
			
			case 6:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong> Please enter Email address.</li></ul>');
			break;
			
			case 5:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong> Please enter Password.</li></ul>');
			break;
			
			case 4:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong> Please enter Phone number.</li></ul>');
			break;
			
			case 3:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong>Please enter valid Email address.</li><li><strong>Error:</strong>Please enter valid Phone Number.</li></ul>');
			break;
			
			case 2:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong>Please enter valid Email address.</li></ul>');
			break;
			
			case 1:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong>An account is already registered with your email address. Please log in.</li></ul>');
			break;
			
			case 11:
			echo $error = _e('<ul class="woocommerce-error"><li><strong>Error:</strong>Please enter valid Phone number.</li></ul>');
			break;
			
			default:
			break;
			
	}
}
?>



<div class="u-columns col2-set" id="customer_login">

<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">

<h2><?php _e( 'Register', 'woocommerce' ); ?></h2>

		<form method="post" class="register" action="<?php echo home_url().'/otp-generate'; ?>">

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
					
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" required value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( $_POST['username'] ) : ''; ?>" />
				</p>

			<?php endif; ?>

			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="reg_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" required value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( $_POST['email'] ) : ''; ?>" />
			</p>
            
			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" value="" name="password" id="reg_password" required />
				</p>

			<?php endif; ?>
            
            <p class="form-row form-row-wide">

       <label for="reg_billing_phone"><?php _e( 'Phone', 'woocommerce' ); ?> <span class="required">*</span></label>

       <input type="text" class="input-text" min="10" max="11" name="phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['phone'] ) ) esc_attr_e( $_POST['phone'] ); ?>" onkeypress="return isNumberKey(event)" onKeyUp="limitText(this,10)" required />

       </p>
			
			<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" autocomplete="off" /></div>



			<p class="woocomerce-FormRow form-row">
				<?php //wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
				<input type="submit" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>" />
			</p>



		</form>
        </div>
         </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        
		
<?php get_footer(); ?>