<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package 7up-framework
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="wrap">
<?php
      $page_id = s7upf_get_value_by_id('s7upf_header_page');
        if(!empty($page_id)){
           s7upf_get_header_visual($page_id); ?>
<div class="container-fluid color-bg-set">
<div class="row">           
<div class="container">           
<div class="vc_row wpb_row header-nav9">
  <div class="wpb_column column_container col-sm-3">
    <div class="vc_column-inner ">
      <div class="wpb_wrapper">
        <div class="wrap-cat-icon wrap-cat-icon-hover wrap-cat-icon6">
          <h2 class="title14 white bg-color title-cat-icon">CATAGORIES</h2>
          <div class="wrap-list-cat-icon">
            <?php wp_nav_menu( array( 'theme_location' => 'primary',
								'menu_class'     => 'list-cat-icon',
								'walker'         => new Custom_Menu_Create	
            ) );
            ?>
            
			<?php //my_menu_layout('primary') ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

<?php }
        else{
            s7upf_get_header_default();
        } ?>
<?php

