<?php
/*function my_menu_layout($theme_location)
{
	$menu = wp_get_nav_menu_object('Header Menu');
    $menu_items = wp_get_nav_menu_items($menu->term_id);
	
	
	
	foreach($menu_items as $menu){
		
		if($menu->menu_item_parent=='0'){
			?>
			<li class="has-cat-mega">
            	<a href="<?php get_site_url();?>">
					<?php echo $menu->post_title; $menu_id= $menu->ID; ?>
                </a>
            </li>
           
            
            <?php
			foreach($menu_items as $menu){
				if($menu->menu_item_parent== $menu_id)
			   	{?>
			   	<h5 class="title-custom-cat-mega-menu"><?php echo $menu->post_title; $menu_parent=$menu->ID;  ?></h5>
					<?php foreach($menu_items as $menu){ ?>
							<ul>
								<?php if($menu->menu_item_parent==$menu_parent){ ?>
		                            	<li><a href="#"><?php echo $menu->post_title ?></a></li>
                                <?php } ?>
                            </ul>
					<?php } ?>
			   
			   	<?php
			   	}
			} ?>
			
			
            
            <?php 
            	
		}
		else
		{
			echo "hi";
		}
		
	}
	
    
    
    
    

	

    
}*/

class Custom_Menu_Create extends Walker_Nav_Menu{
	
    function start_lvl( &$output, $depth = 0, $args = array() ) {
		print_r($output);
		if( 1 == $depth ){return;}
		
			$indent = str_repeat("\t", $depth);
			$output .= "\n$indent<div class='cat-mega-menu cat-mega-style1'> 
						<div class='vc_row wpb_row'><div class='wpb_column column_container col-sm-3'><div class='vc_column-inner'>
						<div class='wpb_wrapper'><div class='wpb_text_column wpb_content_element list-cat-mega-menu'><div class='wpb_wrapper'><ul>";
	
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
		if( 1 == $depth ){return;}
			$output .= "</ul></div></div></div></div></div></div></div>";
    }
}




add_filter( 'woocommerce_login_redirect', 'ckc_login_redirect', 10, 2 );
function ckc_login_redirect( $redirect_url, $user) {
    if( $user->roles[0] == 'seller' ) {
       return dokan_get_navigation_url('/dashboard');
    }
return $redirect_url;
}
function my_login_logo() { ?>
<style type="text/css">
#login h1 a, .login h1 a {
background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/site-login-logo.png);
height:100px;
width:200px;
background-size: 200px 100px;
background-repeat: no-repeat;
}

.login-action-login{
background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/background.jpg);
background-repeat:repeat;
}

#login {
width: 385px;
}

#login form{
background: rgba(000,000,000,0.3);
box-shadow: 10px 10px 10px 2px rgba(0,0,0,.13);
}

.login label{
color: #ffffff !important;
}

.login form .input, .login form input[type=checkbox], .login input[type=text] {
background: rgba(169, 147, 150, 0.31) !important;
}

input[type=text], input[type=search], input[type=radio], input[type=tel], input[type=time], input[type=url], input[type=week], input[type=password], input[type=checkbox], input[type=color], input[type=date], input[type=datetime], input[type=datetime-local], input[type=email], input[type=month], input[type=number], select, textarea {
border: 1px solid #75696a !important;
-webkit-box-shadow: inset 0 3px 4px rgba(0,0,0,.07) !important;
box-shadow: inset 0 3px 4px rgba(0,0,0,.07) !important;
color: #000000 !important;
}
</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Bookmyparts.com';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

if(!function_exists('s7upf_get_header_visual')){
    function s7upf_get_header_visual($page_id){
        ?>
        <div id="header" class="header-page">
            <div class="container-fluid topbar">
              <div class="container">
              <div class="vc_row wpb_row top-header4">
                <div class="wpb_column column_container col-sm-6">
                <div class="vc_column-inner ">
        		<div class="wpb_wrapper">
        			<div class="account-login">
                <?php if(is_nav_menu('top area')){ wp_nav_menu( array( 'menu' => 'top area') ); } ?>
              </div>
        		</div>
        </div>
      </div>
        <div class="wpb_column column_container col-sm-6"><div class="vc_column-inner">
        		<div class="wpb_wrapper">
        			<div class="account-login account-login6">

                <?php if(is_user_logged_in()){?>
                <a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-lock"></i><span class="no-style">Logout</span></a>
                <?php if(is_user_logged_in() && current_user_can("customer")){ ?>
              <div class="currency-language">
              <div class="currency-box custom-drp">
              <p><a class="currency-current" href="<?php echo get_the_permalink(get_option('woocommerce_myaccount_page_id')); ?>"><i class="fa fa-check-square"></i>&nbsp;<span class="no-style">  My Account</span></a></p>
              <ul class="currency-list list-unstyled">
              <li><a href="<?php echo get_the_permalink(); ?>my-order">My Orders</a></li>
              <li><a href="<?php echo get_the_permalink(); ?>order-tracking">Order Tracking</a></li>
              </ul>
              </div>
              </div>
              <?php }elseif(current_user_can("seller")){ ?>
                <a href="<?php echo get_the_permalink(); ?>dashboard"><i class="fa fa-check-square"></i>&nbsp;<span class="no-style">  My Account</span></a>
                <?php } else { ?>
                  <a href="<?php echo get_the_permalink(get_option('woocommerce_myaccount_page_id')); ?>"><i class="fa fa-check-square"></i>&nbsp;<span class="no-style">  My Account</span></a>
                  <?php } ?>

                <?php }else {?>
                  <a href="<?php echo get_the_permalink(get_option('woocommerce_myaccount_page_id')); ?>?action=seller"><i class="fa fa-user"></i><span class="no-style">Seller Registration</span></a>
                  <a href="<?php echo get_the_permalink(get_option('woocommerce_myaccount_page_id')); ?>?action=login"><i class="fa fa-lock"></i><span class="no-style">Login</span></a>
                  <a href="<?php echo get_the_permalink().'register'; ?>"><i class="fa fa-user-plus"></i><span class="no-style">Register</span></a>

                  <?php } ?>
                  </div>
  </div>
        		</div>
        </div>
      </div>
    </div>
      </div>

        <div class="container">
                <?php echo S7upf_Template::get_vc_pagecontent($page_id);?>
            </div>
          </div>
        <?php
    }
}




//**Woocommerce Billing details add extra field hook
add_filter('woocommerce_billing_fields', 'custom_woocommerce_billing_fields');
function custom_woocommerce_billing_fields($fields)
{
    $fields['billing_gst_number'] = array(
        'label' => __('GST Number', 'woocommerce'), // Add custom field label
        'placeholder' => _x('GST Number', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'required' => false, // if field is required or not
        'clear' => false, // add clear or not
        'type' => 'text', // add field type
        'class' => array('custom-gst-number')   // add class name
    );

	/*$fields['billing_ph_ver'] = array(
                'label' => __('OTP Verification', 'woocommerce'), // Add custom field label
                'placeholder' => _x('OTP Verification', 'placeholder', 'woocommerce'), // Add custom field placeholder
                'required' => true, // if field is required or not
                'clear' => false, // add clear or not
                'type' => 'text', // add field type
                'class' => array('form-row-wide custom-otp-number')    // add class name
            );*/

    return $fields;
}


//**wp-admin user Profile GST Number update hook
add_action('edit_user_profile_update', 'update_extra_profile_fields');
function update_extra_profile_fields($user_id) {
 if ( current_user_can('edit_user',$user_id) )
 	$user_meta = get_userdata($user_id);
	$user_roles = $user_meta->roles;

 	if($user_roles[0] == 'seller'):
	 update_user_meta($user_id, 'gst_number', $_POST['gst_number']);
	else:
	 update_user_meta($user_id, 'billing_gst_number', $_POST['billing_gst_number']);
	endif;
}


//**Woocommerce seller and customer Validation function
function create_user_validation_check($email, $password, $phone, $role){
  	if($role=='seller'){

		$validate_email = 0;
		$validate_phone = 0;
		$empty_email = 0;
		$empty_phone = 0;
		$empty_password = 0;
		$email_exists = 0;

		if (empty( $email )) {
			$empty_email = 1;
		}

		if(!is_email( $email ))		{
			$validate_email = 1;
		}

		if (email_exists( $email ) ) {
			$email_exists = 1;
		}

		if (empty( $password ) ) {
			$empty_password = 1;
		}

		if (empty($phone)) {
			$empty_phone = 1;
		}

		if (strlen($phone)< 10) {
			$validate_phone = 1;
		}

		switch(true)
		{
		  	case ($empty_email == 1 and $empty_phone == 1 and $empty_password == 1):
			wp_redirect(home_url().'/account/?action=seller&err=10');
			break;

		  	case ($empty_email == 1 and $empty_phone == 1 ):
			wp_redirect(home_url().'/account/?action=seller&err=9');
			break;

			case ($empty_phone == 1 and $empty_password == 1 ):
			wp_redirect(home_url().'/account/?action=seller&err=8');
			break;

			case ($empty_email == 1 and $empty_password == 1 ):
			wp_redirect(home_url().'/account/?action=seller&err=7');
			break;

		 	case ($empty_email == 1 ):
			wp_redirect(home_url().'/account/?action=seller&err=6');
			break;

			case ($empty_password == 1):
			wp_redirect(home_url().'/account/?action=seller&err=5');
			break;

			case ($empty_phone == 1):
			wp_redirect(home_url().'/account/?action=seller&err=4');
			break;

			case ($validate_email == 1 and $validate_phone == 1):
			wp_redirect(home_url().'/account/?action=seller&err=3');
			break;

		  	case ($validate_email == 1  ):
			wp_redirect(home_url().'/account/?action=seller&err=2');
			break;

			case ($email_exists == 1  ):
			wp_redirect(home_url().'/account/?action=seller&err=1');
			break;

			case ($validate_phone == 1 ):
			wp_redirect(home_url().'/account/?action=seller&err=11');
			break;

			default:
			break;
		}
	}
	else {

		$validate_email = 0;
		$validate_phone = 0;
		$empty_email = 0;
		$empty_phone = 0;
		$empty_password = 0;
		$email_exists = 0;


		if (empty( $email )) {
			$empty_email = 1;
		}

		if(!is_email( $email ))		{
			$validate_email = 1;
		}

		if (email_exists( $email ) ) {
			$email_exists = 1;
		}

		if (empty( $password ) ) {
			$empty_password = 1;
		}

		if (empty($phone)) {
			$empty_phone = 1;
		}

		if (strlen($phone)< 10) {
			$validate_phone = 1;
		}

		switch(true)
		{
		  	case ($empty_email == 1 and $empty_phone == 1 and $empty_password == 1):
			wp_redirect(home_url().'/register?err=10');
			break;

		  	case ($empty_email == 1 and $empty_phone == 1 ):
			wp_redirect(home_url().'/register?err=9');
			break;

			case ($empty_phone == 1 and $empty_password == 1 ):
			wp_redirect(home_url().'/register?err=8');
			break;

			case ($empty_email == 1 and $empty_password == 1 ):
			wp_redirect(home_url().'/register?err=7');
			break;

		 	case ($empty_email == 1 ):
			wp_redirect(home_url().'/register?err=6');
			break;

			case ($empty_password == 1):
			wp_redirect(home_url().'/register?err=5');
			break;

			case ($empty_phone == 1):
			wp_redirect(home_url().'/register?err=4');
			break;

			case ($validate_email == 1 and $validate_phone == 1):
			wp_redirect(home_url().'/register?err=3');
			break;

		  	case ($validate_email == 1  ):
			wp_redirect(home_url().'/register?err=2');
			break;

			case ($email_exists == 1  ):
			wp_redirect(home_url().'/register?err=1');
			break;

			case ($validate_phone == 1 ):
			wp_redirect(home_url().'/register?err=11');
			break;

			default:
			break;
		}

	}
}

//**Woocommerce add customer and seller registration
function add_new_user($user_data) {
	$email = $user_data['email'];
	$password = $user_data['password'];
	$phone= $user_data['phone'];
	$gstno= $user_data['gst'];
	$fname= $user_data['username'];
	$storename= $user_data['company_name'];
	$role= $user_data['role'];
	$username = sanitize_user( current( explode( '@', $email ) ), true );
    $customer_id = username_exists( $username );

    if ( !$customer_id && email_exists($email) == false ) {
        $customer_id = wp_create_user( $username, $password, $email );
        if( !is_wp_error($customer_id) ) {

            if($role=='customer'){
            $user = get_user_by( 'id', $customer_id );
            $user->set_role('customer');

            }
            if($role=='seller'){
            $user = get_user_by( 'id', $customer_id );
            $user->set_role('seller');
            $email            = $email;
              $pwd              = $password;
              $first_name       = $fname;
              $store_name       = $storename;
              $phone            = $phone;
              $newUserId      = wp_update_user( array( 'ID'=> $customer_id,
                  'role'       => 'seller',
                  'first_name' => $first_name
              ) );
              $newUser        = get_user_by( 'email', $email );
              $dokan_settings = array(
                  'store_name'     => $storename,
                  'social'         => array(),
                  'payment'        => array(),
                  'phone'          => $phone,
                  'show_email'     => 'no',
                  'location'       => '',
                  'find_address'   => '',
                  'dokan_category' => '',
                  'banner'         => 0,
              );
			  update_user_meta( $newUserId, 'gst_number', $gstno );
              update_user_meta( $newUserId, 'dokan_enable_selling', 'yes' );
              update_user_meta( $newUserId, 'dokan_profile_settings', $dokan_settings );
              update_user_meta( $newUserId, 'dokan_store_name', $dokan_settings['store_name'] );

            }
  	update_user_meta( $customer_id, 'billing_phone', $phone );
	$session = apply_filters( 'attach_session_information', array(), $customer_id );
    $session['expiration'] = 3*60*60;
   // IP address.
    if ( !empty( $_SERVER['REMOTE_ADDR'] ) ) {
        $session['ip'] = $_SERVER['REMOTE_ADDR'];
    }
   // User-agent.
    if ( ! empty( $_SERVER['HTTP_USER_AGENT'] ) ) {
        $session['ua'] = wp_unslash( $_SERVER['HTTP_USER_AGENT'] );
    }
   // Timestamp
   $session['login'] = time();
   $token = wp_generate_password( 43, true, true );
	update_user_meta( $customer_id, 'session_tokens', $token);
	wp_set_auth_cookie( $customer_id, true );

              if($role=='customer'){
              wp_redirect(home_url().'/account');
              }
              if($role=='seller'){
              wp_redirect(home_url().'/dashboard');
              }
    }
	}

}


/**
 * Process the checkout
**/

/*add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process() {
   wp_nonce_field( 'woocommerce-process_checkout' );
}


//Mobile OTP check in checkout Form
add_action('wp_ajax_my_action', 'my_action');
add_action('wp_ajax_nopriv_my_action', 'my_action');

function my_action(){

	$number = $_POST['phone'];
	$account_request= $_POST['check'];
	$otp_get=$_POST['otp'];
	$check_var=$_POST['checkvar'];

    if(strlen($number) > 10 || strlen($number) < 10 || empty($number)){ echo 'null';}
    session_start();
    if(strlen($number) == 10 && $account_request=='1' && empty($otp_get) && $check_var!='1'){
	$string = '0123456789';
	$string_shuffled = str_shuffle($string);
	$otp = substr($string_shuffled, 1, 6);

	$auto_genrated = md5($otp);
	$message = urlencode("Your OTP is ".$otp);
	$senderid= "INFOSM";
	echo $message.'<br>';


    $request_url ='http://103.233.79.246//submitsms.jsp';
      //$post_data = 'user=BOOKMYPA&key=01f86d3047XX&mobile='.$phone.'&message='.$message.'&senderid='.$senderid.'&accusage=1';
    $post = curl_init();
	curl_setopt($post, CURLOPT_URL, $request_url);
	curl_setopt($post, CURLOPT_POST,TRUE);
	curl_setopt($post, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($post, CURLOPT_RETURNTRANSFER, TRUE);

    $auth = curl_exec($post);
      curl_close($post);
      $auth_array = explode(",",$auth);
      $_SESSION['otp']=$otp;
    }
    $_SESSION['number']=$number;
    if(!empty($otp_get) && strlen($otp_get) == 6)
     {
		if($number==$_SESSION['number'] && $account_request=='1' && $otp_get==$_SESSION['otp'])
		 { echo 'valid'; }
		 else { echo 'Invalid';}
	 }

}*/

//button Buy Now add to cart page code here//

add_filter ('woocommerce_add_to_cart_redirect', 'woo_redirect_to_checkout');

function woo_redirect_to_checkout() {

global $woocommerce;

if($_POST['buynow'] == 'Buy Now'){

   $woocommerce->cart->add_to_cart($_POST['buynowpro'],$_POST['quantity']);

   $checkout_url = WC()->cart->get_checkout_url();

   wp_redirect($checkout_url); exit;

 }
}
/* code for custom field add in coupen limit sanket */
// function add_coupon_display_limit() {
// woocommerce_wp_text_input( array('id' => 'display_limit','label' => __( 'Coupon display limit', 'woocommerce' ),'placeholder' => wc_format_localized_price( 0 ),
// 'description' => __( 'Number of coupen display.', 'woocommerce' ),'data_type'=> 'price','desc_tip' => true, ) );
// }
// add_action( 'woocommerce_coupon_options', 'add_coupon_display_limit', 10, 0 );
//
// function save_coupon_display_limit($post_id) {
// //$post_id= $_GET['post'];
// $display_limit = $_POST['display_limit'];
// update_post_meta( $post_id, 'display_limit', $display_limit); }
// add_action( 'woocommerce_coupon_options_save', 'save_coupon_display_limit');

function show_coupens($id,$limit,$cat_id)
{
$Coupen_for_print = array();
  $Coupon_for_data = array('name' => '', 'desc' =>'');
  $count=0;
    $cdate= strtotime(date('Y-m-d')); ?>
    <?php
    $coupons_args = array(
      'posts_per_page'   => -1,
      'orderby'          => 'title',
      'order'            => 'asc',
      'post_type'        => 'shop_coupon',
      'post_status'      => 'publish'
    );
    $coupons = get_posts($coupons_args);
    foreach($coupons as $coup){
      $code = $coup->post_title;
      $coupon_post = get_post($coupon->id);
      $coupon = new WC_Coupon($code);
      $coupon_data = array(
        'id' => $coupon->id,
        'code' => $coupon->code,
        'type' => $coupon->type,
        'usage_limit' => (!empty($coupon->usage_limit) ) ? $coupon->usage_limit : null,
        'usage_count' => (int) $coupon->usage_count,
        'description' => $coupon_post->post_excerpt,
        'product_ids' => array_map('absint', (array) $coupon->product_ids),
        'product_category_ids' => array_map('absint', (array) $coupon->product_categories),
        'expiry_date' => (!empty($coupon->expiry_date) ) ? date('Y-m-d', $coupon->expiry_date) : null,
        );
      if($cdate <= strtotime($coupon->expiry_date ) && (in_array($id,$coupon_data['product_ids'])||(in_array($cat_id,$coupon_data['product_category_ids']))))
      {  $collect_coupen[]=$coupon->id;$product_id_check[]=$id;$count++; }
      }
      foreach( $collect_coupen as $coupen_id ){
        $check_id = array();
        array_push(  $check_id, $coupen_id);
        $coupons_args = array(
            'posts_per_page'   => -1,
            'orderby'          => 'title',
            'order'            => 'asc',
            'post_type'        => 'shop_coupon',
            'post_status'      => 'publish'
          );
          $coupons = get_posts($coupons_args);
          $code = $coupons->post_title;
          $coupon = new WC_Coupon($code);
          $coupon_post = get_post($coupen_id);
          $coupon_data = array(
            'id' => $coupen_id,
            'type' => $coupon->type,
            'description' => $coupon_post->post_excerpt,
          );
          $Coupon_for_data['name']= $coupon_post->post_title;
            $Coupon_for_data['desc']= $coupon_post->post_excerpt;
            array_push($Coupen_for_print , $Coupon_for_data);
          }
          return  $Coupen_for_print; 
}


