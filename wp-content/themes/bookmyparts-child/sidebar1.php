<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package 7up-framework
 */
?>
<?php
$coupons_args = array(
  'posts_per_page'   => -1,
  'orderby'          => 'title',
  'order'            => 'asc',
  'post_type'        => 'shop_coupon',
  'post_status'      => 'publish'
);
$coupons = get_posts($coupons_args);
global $post, $product;
$current_product_id = $product->get_id();
$product_cats_ids = wc_get_product_term_ids( $product->get_id(), 'product_cat' );
foreach($coupons as $coup){
  $code = $coup->post_title;
  $coupon = new WC_Coupon($code);
  $coupon_post = get_post($coupon->id);
  $coupon_data = array(
    'id' => $coupon->id,
    'code' => $coupon->code,
    'type' => $coupon->type,
    'created_at' => $coupon_post->post_date_gmt,
    'updated_at' => $coupon_post->post_modified_gmt,
    'amount' => wc_format_decimal($coupon->coupon_amount, 2),
    'individual_use' => ( 'yes' === $coupon->individual_use ),
    'product_ids' => array_map('absint', (array) $coupon->product_ids),
    'exclude_product_ids' => array_map('absint', (array) $coupon->exclude_product_ids),
    'usage_limit' => (!empty($coupon->usage_limit) ) ? $coupon->usage_limit : null,
    'usage_count' => (int) $coupon->usage_count,
     'expiry_date' => (!empty($coupon->expiry_date) ) ? date('Y-m-d', $coupon->expiry_date) : null,
    'enable_free_shipping' => $coupon->enable_free_shipping(),
    'product_category_ids' => array_map('absint', (array) $coupon->product_categories),
    'exclude_product_category_ids' => array_map('absint', (array) $coupon->exclude_product_categories),
    'exclude_sale_items' => $coupon->exclude_sale_items(),
    'minimum_amount' => wc_format_decimal($coupon->minimum_amount, 2),
    'maximum_amount' => wc_format_decimal($coupon->maximum_amount, 2),
    'customer_emails' => $coupon->customer_email,
    'description' => $coupon_post->post_excerpt,
  );
foreach($coupon_data['exclude_product_ids'] as $exl_product){
    if($exl_product == $current_product_id){ //echo 'exclude product<br>';
    $current_product_id=0;
    show_coupens($current_product_id); }
  }
if(!in_array($current_product_id,$coupon_data['product_ids']))
{
  foreach($coupon_data['product_category_ids'] as $product_cat){
    foreach($product_cats_ids as $all_cat_id){
      if($product_cat== $all_cat_id){
        //echo 'yes Category ID no product id<br>';
        show_coupens($current_product_id);
        } } }
  }
foreach($coupon_data['exclude_product_category_ids'] as $exl_product_cat){
        foreach($product_cats_ids as $all_cat_id){
          if($exl_product_cat== $all_cat_id){ //echo 'exclude category<br>';
          } } }
foreach($coupon_data['product_ids'] as $product_val){
            $usage_left = $coupon_data['usage_limit'] - $coupon_data['usage_count'];
            if($product_val == $current_product_id && $usage_left > 0 ){ //echo 'Product yes coupen valid<br>';
            if($usage_left<=0){//echo 'Coupon Usage Limit Reached<br>';
			}
            show_coupens($current_product_id);
            } }
if(in_array($current_product_id,$coupon_data['product_ids']))
        {
              foreach($coupon_data['product_category_ids'] as $product_cat){
                foreach($product_cats_ids as $all_cat_id){
                  if($product_cat== $all_cat_id)
                  {
                  //echo 'both match <br>';
                  $current_product_id=0;
                  show_coupens($current_product_id);
                  } } }
        }
} ?>
<?php
$sidebar = s7upf_get_sidebar();
if ( is_active_sidebar( $sidebar['id']) && $sidebar['position'] != 'no' ):
?>
<div class="col-md-3 col-sm-4 col-xs-12 sidebar">
<?php  dynamic_sidebar($sidebar['id']); ?>
</div>
<?php endif; ?>
