<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package 7up-framework
 */
?>
<?php
error_reporting(0);
$coupons_args = array(
  'posts_per_page'   => -1,
  'orderby'          => 'title',
  'order'            => 'asc',
  'post_type'        => 'shop_coupon',
  'post_status'      => 'publish'
);

$coupons = get_posts($coupons_args);
$Coupen_for_print = array();
global $post, $product;
$current_product_id = $product->get_id();
$product_cats_ids = wc_get_product_term_ids( $product->get_id(), 'product_cat' );
foreach($coupons as $coup){
  $code = $coup->post_title;
  $coupon = new WC_Coupon($code);
  $coupon_post = get_post($coupon->id);
  $coupon_data = array(
    'id' => $coupon->id,
    'code' => $coupon->code,
    'type' => $coupon->type,
    'created_at' => $coupon_post->post_date_gmt,
    'updated_at' => $coupon_post->post_modified_gmt,
    'amount' => wc_format_decimal($coupon->coupon_amount, 2),
    'display_limit' => wc_format_decimal($coupon->display_limit, 2),
    'individual_use' => ( 'yes' === $coupon->individual_use ),
    'product_ids' => array_map('absint', (array) $coupon->product_ids),
    'exclude_product_ids' => array_map('absint', (array) $coupon->exclude_product_ids),
    'usage_limit' => (!empty($coupon->usage_limit) ) ? $coupon->usage_limit : null,
    'usage_count' => (int) $coupon->usage_count,
     'expiry_date' => (!empty($coupon->expiry_date) ) ? date('Y-m-d', $coupon->expiry_date) : null,
    'enable_free_shipping' => $coupon->enable_free_shipping(),
    'product_category_ids' => array_map('absint', (array) $coupon->product_categories),
    'exclude_product_category_ids' => array_map('absint', (array) $coupon->exclude_product_categories),
    'exclude_sale_items' => $coupon->exclude_sale_items(),
    'minimum_amount' => wc_format_decimal($coupon->minimum_amount, 2),
    'maximum_amount' => wc_format_decimal($coupon->maximum_amount, 2),
    'customer_emails' => $coupon->customer_email,
    'description' => $coupon_post->post_excerpt,
  );

foreach($coupon_data['exclude_product_ids'] as $exl_product)
{
    if($exl_product == $current_product_id)
    {
      //echo 'exclude product<br>';
      $current_product_id=0;
      $exclude_pro_id[]=$exl_product;
      array_push($Coupen_for_print, show_coupens($current_product_id,$limit,$all_cat_id) );
    }
}

if(!in_array($current_product_id,$coupon_data['product_ids']))
{
  foreach($coupon_data['product_category_ids'] as $product_cat)
  {
        foreach($product_cats_ids as $all_cat_id)
        {
          if($product_cat== $all_cat_id)
          { //echo 'yes Category ID no product id<br>';
            $product_cat_check[]=$product_cat;
          }
        }
  }
}
foreach($coupon_data['exclude_product_category_ids'] as $exl_product_cat)
{
  foreach($product_cats_ids as $all_cat_id)
  {
    if($exl_product_cat== $all_cat_id)
    {
      //echo 'exclude category<br>';
      $exclude_cat_id[]=$exl_product_cat;

    }
  }
}
foreach($coupon_data['product_ids'] as $product_val)
{
  $product_id_check[]=$product_val;
}
}
if(!isset($exclude_pro_id)){
$pcheck=array_unique($product_id_check);
foreach($pcheck as $c_product_id)
{ $post = get_post_meta($c_product_id);
  $custom_request=$post['coupen_display_limit'][0];
  if($current_product_id==$c_product_id){
    array_push($Coupen_for_print, show_coupens($c_product_id,$limit,$all_cat_id) );
  } } }

if(!isset($exclude_pro_id))
{
$ccheck=array_unique($product_cat_check);
foreach($ccheck as $c_product_cat)
{
array_push($Coupen_for_print, show_coupens($c_product_id,$limit,$c_product_cat));
}
}
$temp = array();
for($k=0;$k<count($Coupen_for_print);$k++)
{
  for($j=0;$j<count($Coupen_for_print[$k]);$j++)
  {
    array_push($temp,$Coupen_for_print[$k][$j]);
  }
}
$post = get_post_meta($current_product_id);
$custom_request=$post['coupen_display_limit'][0];
if(isset($custom_request)){ $limit = $custom_request;}
else { $limit=5; }
$coupen_unique = array_map("unserialize", array_unique(array_map("serialize", $temp)));
if(count($coupen_unique)!=0){
?>
<div class="col-md-3 col-sm-4 col-xs-12 sidebar">
  <div class="coupon-off">
  <h3 class="offer-title">Select from <?php echo $limit; ?> Offers</h3>
<?php
for($i=0;$i<count($coupen_unique);$i++)
{
if($limit>0){
?>
<div class="offer-detail">
  <div class="our-coupon">
    <input class="to-copy" readonly="readonly" value="<?php echo $coupen_unique[$i]['name']; ?>">
    <button id="copy" class="copy-code-btn" type="button">Copy code</button>
  </div>
  <p><?php echo $coupen_unique[$i]['desc']; ?></p>
  <div class="offer-terms">
      <a href="#" title="Terms & Conditions">*Terms & Conditions</a>
  </div>
</div>
<?php $limit--;} } ?>
</div>
</div>
<?php }
$sidebar = s7upf_get_sidebar();
if ( is_active_sidebar( $sidebar['id']) && $sidebar['position'] != 'no' ):
?>
<div class="col-md-3 col-sm-4 col-xs-12 sidebar111">

<?php  dynamic_sidebar($sidebar['id']);?>

</div>
<?php endif; ?>
