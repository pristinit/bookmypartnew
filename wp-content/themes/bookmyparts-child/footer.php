<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package 7up-framework
 */

?>
	    <?php
	    $page_id = s7upf_get_value_by_id('s7upf_footer_page');
	    if(!empty($page_id)) {
	        s7upf_get_footer_visual($page_id);
	    }
	    else{
	        s7upf_get_footer_default();
	    }
	    s7upf_scroll_top();
	    ?>
	    <div class="wishlist-mask">
	    	<?php
	    	if(class_exists('YITH_WCWL_Init')){
		    	$url = YITH_WCWL()->get_wishlist_url();
		    	echo    '<div class="wishlist-popup">
	                        <span class="popup-icon"><i class="fa fa-bullhorn" aria-hidden="true"></i></span>
	                        <p class="wishlist-alert">"<span class="wishlist-title"></span>" '.esc_html__("was added to wishlist","kuteshop").'</p>
	                        <div class="wishlist-button">
	                            <a href="#" class="wishlist-close">'.esc_html__("Close","kuteshop").' (<span class="wishlist-countdown">3</span>)</a>
	                            <a href="'.esc_url($url).'">'.esc_html__("View page","kuteshop").'</a>
	                        </div>
	                    </div>';
	        }
	    	?>
	    </div>
	</div>
<div id="boxes"></div>

<div class="modal produ-lay-form" id="myModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close cancel-btn" data-dismiss="modal" aria-hidden="true"><img src="<?php echo get_template_directory_uri(); ?>/images/close.png" /></button>
                 <h4 class="modal-title" id="myModalLabel">Product Enquiry</h4>

            </div>
            <div class="modal-body"><?php echo do_shortcode('[contact-form-7 id="6" title="Product Inquiry"]'); ?></div>
            
        </div>
    </div>
</div>
<script type="text/javascript">
/*var toCopy  = document.getElementById( 'to-copy' ),
    btnCopy = document.getElementById( 'copy' );
    btnCopy.addEventListener( 'click', function(){
    toCopy.select();
      if ( document.execCommand( 'copy' ) ) {
       document.getElementById('copy').innerHTML="copied";
    }
  return false;
});*/
</script>

<script type="text/javascript">
jQuery("#product_enq").click(function() {
  jQuery("#myModal").fadeIn();
  
});
</script>
<script type="text/javascript">
(function($) {
  $("#uploadtextfield").click(function() {
        $('#fileuploadfield').click()
    });
    $("#uploadbrowsebutton").click(function() {
        $('#fileuploadfield').click()
    });
    $('#fileuploadfield').change(function() {
    $('#uploadtextfield').val($(this).val());
  });
})(jQuery);
</script>
<script type="text/javascript">
/*jQuery('#createaccount').change(function() {
    if(this.checked) {
    document.getElementById("billing_ph_ver_field").style.display = "";
	document.getElementById("otp_text").style.display = "";
    }
    else
    {
    document.getElementById("billing_ph_ver_field").style.display = "none";
	document.getElementById("otp_text").style.display = "none";
    document.getElementById("billing_ph_ver").removeAttribute('required');
    }
});
jQuery('#createaccount').on('click', function () {
    jQuery(this).val(this.checked ? 1 : 0);
});*/


</script>

<script type="text/javascript">
/*
function checkphone() {
        //document.getElementById("account_password").disabled = true;
        var create_account_request= jQuery('#createaccount').val();
        var phone = jQuery('#billing_phone').val();
        var  checkvar= 0;
        jQuery.ajax({
        url: "<?php //echo admin_url('admin-ajax.php'); ?>",
        type: 'POST',
        data:{action:'my_action',check:create_account_request,phone:phone,checkvar:checkvar},
        success: function(response) {
				if(response=='null0'){
					jQuery('#billing_phone_field').addClass('woocommerce-invalid woocommerce-invalid-required-field');				
					jQuery("#billing_phone").after("<div class='phone-er'><strong>Phone </strong> is a required field.</div>");
                }else{
					jQuery('#billing_phone_field').removeClass('woocommerce-invalid woocommerce-invalid-required-field');
					jQuery(".phone-er").remove();
					alert('Got this from the server: ' + response);
				}
		}
        });
}
jQuery('#billing_ph_ver').keyup(function() {
var v = jQuery('#billing_ph_ver').val();
var create_account_request= jQuery('#createaccount').val();
var phone = jQuery('#billing_phone').val();
var checkvar=1;
jQuery.ajax({
        url: "<?php //echo admin_url('admin-ajax.php'); ?>",
        type: 'POST',
        data:{action:'my_action',check:create_account_request,phone:phone,otp:v,checkvar:checkvar},
        success: function(response) {
                //document.getElementById("otp-checks").innerHTML = "OTP SENT!";
                 if(response=='valid0')
                    {
					jQuery(".phone-er").remove();
                    document.getElementById("otp-checks").onclick = '';
                    document.getElementById('otp-checks').setAttribute("onclick", "");
                    document.getElementById('otp-checks').removeAttribute("onclick");
                    }else if(response=='Invalid0'){
					jQuery("#billing_ph_ver").after("<div class='phone-er'>You have entered an invalid <strong> OTP</strong></div>");		
                    document.getElementById('billing_ph_ver').value = "";
                    document.getElementById('otp-checks').setAttribute("onclick", "");
                    document.getElementById('otp-checks').removeAttribute("onclick");                    
                    }
            }
        });
});
*/
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 44)
        return false;
        return true;
}
    
function limitText(limitField, limitNum) {
if (limitField.value.length > limitNum) {
    limitField.value = limitField.value.substring(0, limitNum);
}
}
</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri().'/assets/js/'; ?>jquery.validate.min.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/assets/js/'; ?>register-validate.js" ></script>
<?php wp_footer(); ?>

</body>
</html>
