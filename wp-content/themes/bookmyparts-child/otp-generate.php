<?php
/*
Template Name: OTP Generate
*/
?>
<style>
/*========================================================*/
/*=======================OTP-Form-CSS=====================*/
/*========================================================*/
.form_otp {
	background: rgba(255, 255, 255, 0.9);
	padding: 10px 30px;
	max-width: 320px;
	position: relative;
	top: 250px;
	left: 0;
	margin: 0 auto;
	right: 0;
	border-radius: 4px;
	box-shadow: 0 2px 5px 2px rgba(19, 35, 47, 0.3);
	min-height: 235px;
}
.title-otp {
	font-weight: 700;
	font-size: 22px;
	text-transform: uppercase;
	text-align: center;
	margin-bottom: 25px;
	margin-top: 12px;
}
.form_otp input[type="number"] {
	font-size: 14px;
	-moz-appearance: textfield;
	-webkit-appearance: textfield;
	-o-appearance: textfield;
	display: block;
	padding: 5px 10px;
	border: 1px dashed #ec685e;
	color: #999;
	height: 36px;
	border-radius: 0;
	margin-top: 20px;
	width: 100%;
}
.otp-buttons {
	width: 100%;
	margin: 20px 10px;
}
.margin-right-15 {
	margin-right: 15px;
}
.btn.button-otp {
	font-size: 14px;
	letter-spacing: 0.6px;
	background-color: #F54303;
	border: medium none;
	color: #fff;
	height: 40px;
	cursor: pointer;
	width: 130px;
	transition: all 0.5s ease-out 0s;
	-webkit-transition: all 0.5s ease-out 0s;
	outline: none;
}
.btn.button-otp:hover {
	background: #333 none repeat scroll 0 0;
}
/*=======================OTP-Form-End=====================*/
</style>
<?php
print_r($_POST);

session_start();

if(isset($_POST['role'])=='seller' || $_GET['errotp'] == 'resends' || $_GET['err_sel'] == 'message')
{
	echo"seller";
	if(isset($_GET['errotp']) && $_GET['errotp'] == 'resends' ){
		$username = $_SESSION['name'];
		$email = $_SESSION['email'];
		$password = $_SESSION['password'];
		$phone = $_SESSION['phone'];
		$gst=$_SESSION['gstno'];
		$company_name= $_SESSION['companyname'];
		$role=$_SESSION['role'];

	}elseif(isset($_GET['err_sel']) && $_GET['err_sel'] == 'message'){
		$username = $_SESSION['name'];
		$email = $_SESSION['email'];
		$password = $_SESSION['password'];
		$phone = $_SESSION['phone'];
		$gst=$_SESSION['gstno'];
		$company_name= $_SESSION['companyname'];
		$role=$_SESSION['role'];
	}else{
		$name = $_POST['fname'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$phone = $_POST['billing_phone'];
		$gst=$_POST['gstno'];
		$company_name= $_POST['shopname'];
		$role=$_POST['role'];

		$_SESSION['name'] = $name;
		$_SESSION['email']=$email;
		$_SESSION['password']=$password;
		$_SESSION['phone']=$phone;
		$_SESSION['gstno']=$gst;
		$_SESSION['companyname']=$company_name;
		$_SESSION['role']=$role;
	}
	if(!isset($_GET['errotp']) && !isset($_GET['err_sel']))
	{
		create_user_validation_check($email, $password, $phone,$role);
	}

}
else
{
if(isset($_GET['errotp']) && $_GET['errotp'] == 'resendu' ){
	$username = $_SESSION['username'];
	$email = $_SESSION['email'];
	$password = $_SESSION['password'];
	$phone = $_SESSION['phone'];

}elseif(isset($_GET['err_user']) && $_GET['err_user'] == 'message'){
	$username = $_SESSION['username'];
	$email = $_SESSION['email'];
	$password = $_SESSION['password'];
	$phone = $_SESSION['phone'];
}else{
	$username = $_POST['username'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$phone = $_POST['phone'];

	$_SESSION['username'] = $username;
	$_SESSION['email']=$email;
	$_SESSION['password']=$password;
	$_SESSION['phone']=$phone;

}
			if(!isset($_GET['errotp']) && !isset($_GET['err_user'])){
				create_user_validation_check($email, $password, $phone,$role);
			}
}

if((isset($phone) || isset($_GET['errotp'])) && !isset($_GET['err_user']) && !isset($_GET['err_sel'])){

	$string = '0123456789';
	$string_shuffled = str_shuffle($string);
	$otp = substr($string_shuffled, 1, 6);

	$auto_genrated = md5($otp);
	$message = urlencode("Your OTP is ".$otp);
	$senderid= "INFOSM";
	echo $message;


	$request_url ='http://103.233.79.246//submitsms.jsp';
	//$post_data = 'user=BOOKMYPA&key=01f86d3047XX&mobile='.$phone.'&message='.$message.'&senderid='.$senderid.'&accusage=1';
	$post = curl_init();

	curl_setopt($post, CURLOPT_URL, $request_url);
	curl_setopt($post, CURLOPT_POST,TRUE);
	curl_setopt($post, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($post, CURLOPT_RETURNTRANSFER, TRUE);

	$auth = curl_exec($post);
	curl_close($post);
	$auth_array = explode(",",$auth);
} ?>
<?php
	if(isset($_GET['err_user']) || isset($_GET['err_sel'])){
	$otp = $_SESSION['otp'];
	$auto_genrated = md5($otp);
	echo"<br>";
	echo $otp." is your otp";
	echo '<p class="error otp">Please enter valid One Time Password.</p>';
	}
	echo "<br>";
	echo $_SESSION['otp'] = $otp; ?>
<?php

if(isset($role)=='seller'){

	echo "seller form";
?>
<div class="form_otp">
	  <div class="title-otp">OTP VERIFICATION</div>
	  <form method="POST" action="<?php echo home_url().'/otp-success';?>">
	    <div class="field-wrap">
	      <input type="hidden" name="username" value="<?php echo $_SESSION['name']; ?>">
	      <input type="hidden" name="password" value="<?php echo $_SESSION['password']; ?>">
	      <input type="hidden" name="email" value="<?php echo $_SESSION['email']; ?>">
	      <input type="hidden" name="phone" value="<?php echo $_SESSION['phone']; ?>">
	      <input type="hidden" name="auto_genrated" value="<?php echo $auto_genrated; ?>">
				<input type="hidden" name="gst" value="<?php echo $_SESSION['gstno']; ?>">
				<input type="hidden" name="company_name" value="<?php echo $_SESSION['companyname']; ?>">
				<input type="hidden" name="role" value="<?php echo $_SESSION['role']; ?>">
	      <input type="number" name="otp" id="otp" autocomplete="off" placeholder="Enter Your OTP" required="required">
	    </div>
	    <p>We have already sent one time password to your provided mobile number.</p>
	    <div class="otp-buttons">
	      <button type="submit" class="btn button-otp margin-right-15" name="verify" value="verify"/>
	      Verify
	      </button>
	      <a href="<?php echo home_url().'/otp-generate?errotp=resends'; ?>">
	      <button type="button" class="btn button-otp"/> Resend OTP </button>
	      </a> </div>
	  </form>
	</div>
	</div>
<?php
}
else {
	echo "customer form";
	$role='customer';
?>
<div class="form_otp">
  <div class="title-otp">OTP VERIFICATION</div>
  <form method="POST" action="<?php echo home_url().'/otp-success';?>">
    <div class="field-wrap">
      <input type="hidden" name="username" value="<?php echo $_SESSION['username']; ?>">
      <input type="hidden" name="password" value="<?php echo $_SESSION['password']; ?>">
      <input type="hidden" name="email" value="<?php echo $_SESSION['email']; ?>">
      <input type="hidden" name="phone" value="<?php echo $_SESSION['phone']; ?>">
			<input type="hidden" name="role" value="<?php echo $role; ?>">
      <input type="hidden" name="auto_genrated" value="<?php echo $auto_genrated; ?>">
      <input type="number" name="otp" id="otp" autocomplete="off" placeholder="Enter Your OTP" required="required">
    </div>
    <p>We have already sent one time password to your provided mobile number.</p>
    <div class="otp-buttons">
      <button type="submit" class="btn button-otp margin-right-15" name="verify" value="verify"/>
      Verify
      </button>
      <a href="<?php echo home_url().'/otp-generate?errotp=resendu'; ?>">
      <button type="button" class="btn button-otp"/> Resend OTP </button>
      </a> </div>
  </form>
</div>
</div>
<?php } ?>
