<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>
<?php
$user_id = get_post_meta($this->order->id,'_customer_user',true);
$gst= get_user_meta( $user_id, 'billing_gst_number', true );
$order = wc_get_order( $this->order->id);
?>
<div class="container">
	<div class="title">
		<h2>
			<?php
			echo apply_filters( 'wpo_wcpdf_invoice_title', __( 'Invoice', 'woocommerce-pdf-invoices-packing-slips' ) );
			?>
		</h2>
	</div>
	<div class="row">
		<table class="order-data-addresses">
			<tr>
				<td class="address billing-address">
					<h2 class="sold">Sold By:</h2>
					<?php
					foreach( $order-> get_items() as $item_key => $item_values ):
						$product_id = $item_values->get_product_id(); // the Product id
						$author_id = get_post_field ('post_author', $product_id);
						$company = get_user_meta( $author_id); ?>
						<span class="cdetail">Company Name : <?php echo $company['dokan_store_name'][0]; ?></span><br />
						<span class="cdetail">GST Number : <?php echo $company['gst_number'][0]; ?></span><br />
					<?php endforeach; ?>
				</td>
				<td class="order-data">
					<div class="shop-name">
						<table>
							<?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
							<?php if ( isset($this->settings['display_number']) ) { ?>
								<tr class="invoice-number">
									<th><?php _e( 'Invoice Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
									<td><?php $this->invoice_number(); ?></td>
								</tr>
							<?php } ?>
							<?php if ( isset($this->settings['display_date']) ) { ?>
								<tr class="invoice-date">
									<th><?php _e( 'Invoice Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
									<td><?php $this->invoice_date(); ?></td>
								</tr>
							<?php } ?>
							<tr class="order-number">
								<th><?php _e( 'Order Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
								<td>#<?php $this->order_number(); ?></td>
							</tr>
							<tr class="order-date">
								<th><?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
								<td><?php $this->order_date(); ?></td>
							</tr>
							<?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<hr />
	<div class="row">
		<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>
		<table class="order-data-addresses">
			<tr>
				<td class="address billing-address">
					<h2 class="ship">Billing Address</h2>
					<!-- <h3><?php _e( 'Billing Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3> -->
					<?php $this->billing_address(); ?>
					<br />
					<?php if (isset($gst) &&  !empty($gst)) { ?>
						GSTIN: <?php echo $gst; ?>
					<?php } ?>
				</td>
				<td class="order-data">
					<?php if (isset($this->settings['display_shipping_address']) && $this->ships_to_different_address()) { ?>
                    <h2 class="ship">Shipping Address</h2>
                    <?php $this->shipping_address(); ?>
					<br />
					<?php if ( isset($gst) &&  !empty($gst)) { ?>
						GSTIN: <?php echo $gst; ?>
					<?php } ?>
                    <?php } ?>
				</td>
			</tr>
		</table>
	</div>
	<div class="order_box">
		<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>
		<table class="order-details">
			<thead>
				<tr align="center">
					<th class="srno"><?php _e('Srno', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<th class="product"><?php _e('Item', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<th class="price"><?php _e('Unit price', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<th class="quantity"><?php _e('Quantity', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<th class="price"><?php _e('Amount', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<th class="gsts">
						<table class="gstd">
							<tr>
								<td colspan="2" class="gst"><?php _e('GST', 'woocommerce-pdf-invoices-packing-slips' ); ?>
								</td>
							</tr>
							<tr>
								<td><?php _e('Rate', 'woocommerce-pdf-invoices-packing-slips' ); ?></td>
								<td><?php _e('Price', 'woocommerce-pdf-invoices-packing-slips' ); ?></td>
							</tr>
						</table>
					</th>
					<th class="price"><?php _e('Total', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php $items = $this->get_order_items();$h=0; if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) :
					$h++; ?>
					<tr id="contain-data" class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->type, $this->order, $item_id ); ?>">
						<td><?php echo $h;?></td>
						<td class="product">
							<?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
							<span class="item-name"><?php echo $item['name']; ?></span>
							<?php do_action( 'wpo_wcpdf_before_item_meta', $this->type, $item, $this->order  ); ?>
							<span class="item-meta"><?php echo $item['meta']; ?></span>
							<dl class="meta">
								<?php $description_label = __( 'SKU', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
								<?php if( !empty( $item['sku'] ) ) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo $item['sku']; ?></dd><?php endif; ?>
								<?php if( !empty( $item['weight'] ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="weight"><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
							</dl>
							<?php do_action( 'wpo_wcpdf_after_item_meta', $this->type, $item, $this->order  ); ?>
						</td>
						<td class="quantity"><?php echo $item['single_price']; ?></td>
						<td class="quantity"><?php echo $item['quantity']; ?></td>
						<td class="price"><?php echo $item['order_price']; ?></td>
						<td class="product">
							<table class="gst_detail">
								<tr>
									<td class="price"><?php echo $item['tax_rates']; ?></td>
									<td class="price"><?php echo $item['line_subtotal_tax']; ?></td>
								</tr>
							</table>
						</td>
						<td class="quantity"><?php echo $item['price']; ?></td>
					</tr>
				<?php endforeach; endif; ?>
				<tr id="contain-data">
					<td></td>
					<td class="product">Shipping charge</td>
					<td colspan="4"></td>
					<td ><?php echo $order->shipping_total; ?>.00</td>
				</tr>
				<tr id="contain-data">
					<td colspan="4"></td>
					<td colspan="2"><b>Grand Total:<b></td>
						<td ><b><?php echo $order->total; ?></b></td>
					</tr>
				</tbody>
			</table>
		</div>
		<table class="owner-data-addresses">
			<tr>
				<td class="owner-phone">
					<p class="phone"><b><?php $phone=$this->settings[extra_1];
					echo $phone['default'];?></b></p>
				</td>
				<td class="owner-email">
					<p class="support"><b><?php $email=$this->settings[extra_2];
					echo $email['default'];?></b></p>
				</td>
				<td class="owner-detail">
					<p class="logo"><?php $this->header_logo();?></p>
				</td>
			</tr>
		</table>
		<div class="term">
			<p><b>Return policy:</b> We try to deliver perferctly each and every time.but in case of you need to return please do so with the <b> Original Packing, Tag and invoice </b> without whichit will be really difficult for as to act on your request. Please help us in helping you.</p>
			<p>* Terms and Condition Apply</p>
		</div>
	</div>
	<?php if ( $this->get_footer() ): ?>
		<div id="footer">
			<?php $this->footer(); ?>
		</div><!-- #letter-footer -->
	<?php endif; ?>
	<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>
