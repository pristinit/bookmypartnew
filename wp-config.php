<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bookmypartnew_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'a/W[g7=A@M<AJg]Iy[s^@d{dGk{bgu?19LsBBcg_!y2pNf:t{;Tn{EbBKKWF^u1|');
define('SECURE_AUTH_KEY',  '_..qG)?;nrh`)s}=t#|wW%iR!ZUFWE|L=naG,,h_ifk^Tik/#aTiuP0gjlpjhb71');
define('LOGGED_IN_KEY',    'o<ij3j9dYyw(ht,$5BSb.:p-dC{IGvGin{oZIZ*aJN?~J}-1&vY5?J?!^U$_l!A9');
define('NONCE_KEY',        '#G$z>{8j{ ;toNY27pGcl.9@?.7]Fo$Q!btr_Sr(h$ ?*S1QQ25aQJ 7>tvFm?FJ');
define('AUTH_SALT',        '3&vZJ@kVKr`)B.,uCyZwm2CfwZ# Az+n|,]n{<pC$@C>T0S5$7uUijBvf_)3mPk=');
define('SECURE_AUTH_SALT', 'a%_l&k5?Whn!)zagRY)!kdmlw{[diM>E82V1c,9y`W% ;Ut*q|Wsg|[9b&h)0U,z');
define('LOGGED_IN_SALT',   'Ea0&-t-y%!=9$^~?8!xoC{t,HNjooDZj@0d91Z4lDs{.BilOjFUq*HU`Ss#}M+>F');
define('NONCE_SALT',       'nujG[eJqT[]s*AkfH4C7-z0gR7O9&f)E=.Pjf2j=l2]R.@s[pB,,XT<3rDI!6LAo');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
